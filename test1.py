import sys
import string

argvs = sys.argv
argc = len(argvs)

if argc <= 0:
    print ('no args!')
    quit()

if type('string') is argvs[1]:
    print ('invalid args!!')
    quit()

if len(argvs[1]) <= 0:
    print ('no args!')
    quit()

if argvs[1].isdecimal():
    limit = int(argvs[1])
else:
    print ('invalid args!')
    quit()

for i in range(limit):
    out = ''

    if i % 3 == 0 and i % 5 == 0:
        out = 'fizzbazz'
    elif i % 3 == 0:
        out = 'fizz'
    elif i % 5 == 0:
        out = 'buzz'
    
    print (str(i) + ' ' + out)

