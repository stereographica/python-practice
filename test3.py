# get json from api urllib.request version
# get holiday in 10 days

import urllib.request
import json
import datetime


def get_date_interval(day_after):
    now = datetime.datetime.now()
    days = []
    for i in range(day_after) :
         days.append(now + datetime.timedelta(i))

    return days


url = 'https://holidays-jp.github.io/api/v1/date.json';

try:
    req = urllib.request.Request(url)

    with urllib.request.urlopen(req) as res:
        body = res.read()

except urllib.error.HTTPError as err:
    print(err.code)
except urllib.err.URLError as err:
    print(err.reason)
except:
    print('Unknown Error Occured.')

json = json.loads(body)

days = get_date_interval(10)

for day in days :
    str_day = day.strftime('%Y-%m-%d')
    if str_day in json:
        print(str_day + " is holiday :" + json[str_day])

